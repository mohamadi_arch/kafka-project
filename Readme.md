# Readme


## Git host
https://gitlab.com/mohamadi_arch/kafka-project

# Show data
we have two way to show data in browser:
1.pgadmin ==> localhost:5000
2.nocodb  ==> localhost:8080

information:
host: postgres
user: postgres
password: 1234
port: 5432
email-for-login: admin@admin.com
password-for-login: pgadmin


## backup

#### 1. basebackup of a whole running db

```sql
pg_basebackup -U postgres -h postgres -D "/etc/backups" -Ft -z -P -Xs  
```

#### 2. full backup

```sql
pg_dumpall | gzip > /opt/user_backups/clusterall_bkz.gz 
```

#### 3. wall archiving

change this items in postgres.conf file
```bash
wal_level=replica
archive_mode = on
archive_command = 'copy "%p" "C:\\archieve\\%f" '    # copy wal into archive windows  %f=name of the file
archive_command = 'cp -i %p /opt/archieve/%f'       # copy wal into archive linux
```
and then run this commands:

```sql
select pg_start_backup('test1')  
tar -cvzf ./backup.tar /var/lib/psql/14/data 
select pg_stop_backup('f')   
```

#### 4. offline backup (not recommend)
```sql
pg_ctl stop                                           --  stop cluster
tar -cvzf ./backup.tar /car/lib/psql/14/data .         -- tar backup the data directly
```