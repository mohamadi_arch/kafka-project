import uuid
import json
import time
import logging
from threading import Thread
from datetime import datetime
import random
import psycopg2
from kafka import KafkaProducer
from kafka import KafkaConsumer
import requests




def get_data():

    res = requests.get("https://randomuser.me/api/")
    res = res.json()
    res = res['results'][0]
    # print(res)
    return res

def format_data(res):
    data = {}
    location = res['location']
    # data['id'] = uuid.uuid4()
    data['first_name'] = res['name']['first']
    data['last_name'] = res['name']['last']
    data['gender'] = res['gender']
    data['address'] = f"{str(location['street']['number'])} {location['street']['name']}, " \
                      f"{location['city']}, {location['state']}, {location['country']}"
    data['post_code'] = location['postcode']
    data['email'] = res['email']
    data['username'] = res['login']['username']
    data['dob'] = res['dob']['date']
    data['registered_date'] = res['registered']['date']
    data['phone'] = res['phone']
    data['picture'] = res['picture']['medium']
    # print(data)
    return data

def stream_data():


    curr_time = time.time()

    while True:
        if time.time() > curr_time + 60: #1 minute
            break
        try:
            res = get_data()
            res = format_data(res)

            producer.send('users_info', json.dumps(res).encode('utf-8'))
        except Exception as e:
            logging.error(f'An error occured: {e}')
            continue


def add_timestamp():

    consumer = KafkaConsumer('users_info',bootstrap_servers=['kafka:9092'])
    for message in consumer:
        print(message)
        print("---------------------------------------")
        string_message =message.value.decode('utf-8')
        json_object = json.loads(string_message)
        timestamp =time.time()
        json_object["timestamp"]=timestamp
        # print(json_object)
        # print(type(json_object))
        producer.send('members_info', json.dumps(json_object).encode('utf-8'))


def add_tags():
    consumer = KafkaConsumer('members_info',bootstrap_servers=['kafka:9092'])
    for message in consumer:
        string_message =message.value.decode('utf-8')
        json_object = json.loads(string_message)
        tags= ["css","js","cpp","py","lua","bash",".net"]
        json_object["tags"]= tags[random.randint(0, len(tags)-1)]
        # print(json_object)
        producer.send('programers_info', json.dumps(json_object).encode('utf-8'))


def save_to_database():
    print("datttttttttttttttttttttttttt")
    consumer = KafkaConsumer('programers_info',bootstrap_servers=['kafka:9092'])


    print(conn, "conn")

    sql = """CREATE TABLE IF NOT EXISTS developers (
             customer_id SERIAL PRIMARY KEY,
             first_name VARCHAR(255) NOT NULL,
             last_name VARCHAR(255) NOT NULL,
             gender VARCHAR(255) NOT NULL,
             address VARCHAR(255) NOT NULL,
             post_code VARCHAR(255) NOT NULL,
             email VARCHAR(255) NOT NULL,
             username VARCHAR(255) NOT NULL,
             dob VARCHAR(255) NOT NULL,
             registered_date VARCHAR(255) NOT NULL,
             phone VARCHAR(255) NOT NULL,
             picture VARCHAR(255) NOT NULL,
             timestamp VARCHAR(255),
             tags VARCHAR(255)
         );"""
    cursor.execute(sql)
    conn.commit()
    for message in consumer:
        string_message =message.value.decode('utf-8')
        json_object = json.loads(string_message)
        print(json_object)
            # cursor.execute('''SELECT * FROM developers;''')
        sql = "INSERT INTO developers (first_name, last_name, gender, address, post_code, \
            email, username, dob, registered_date, phone, picture,tags,timestamp) \
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
        cursor.execute(sql, (json_object['first_name'], json_object['last_name'], json_object['gender'],
                        json_object['address'], json_object['post_code'], json_object['email'],
                            json_object['username'], json_object['dob'], json_object['registered_date'],
                            json_object['phone'], json_object['picture'], json_object['tags'], json_object['timestamp']))
        conn.commit()
        count = cursor.rowcount
        print(count, "Record inserted successfully into mobile table")
        # cursor.close()
        # conn.close()



conn = psycopg2.connect(database="postgres", user="postgres", host="postgres",
                    password="1234", port="5432")
cursor = conn.cursor()
producer = KafkaProducer(bootstrap_servers=['kafka:9092'], max_block_ms=5000)
consumer1 = Thread(target=stream_data)
consumer2 = Thread(target=add_timestamp)
consumer3 = Thread(target=add_tags)
consumer4 = Thread(target=save_to_database)

consumer1.start()
consumer2.start()
consumer3.start()
consumer4.start()

consumer1.join()
consumer2.join()
consumer3.join()
consumer4.join()
        
# get_data()